import re                                 # regular expression
import string                             # for string operations
from porterStemmer import stem

stopwords = ['a', 'about', 'above', 'after', 'again', 'against', 'all', 'am', 'an', 'and', 'any',
 'are', 'as', 'at', 'be', 'because', 'been', 'before', 'being', 'below', 'between', 'both', 
 'but', 'by', 'can', 'did', 'do', 'does', 'doing', 'don', 'down', 'during', 'each', 'few', 'for',
  'from', 'further', 'had', 'has', 'have', 'having', 'he', 'her', 'here', 'hers', 'herself', 
  'him', 'himself', 'his', 'how', 'i', 'if', 'in', 'into', 'is', 'it', 'its', 'itself', 'just',
   'me', 'more', 'most', 'my', 'myself', 'no', 'nor', 'now', 'of', 'off', 'on', 'once',
    'only', 'or', 'other', 'our', 'ours', 'ourselves', 'out', 'over', 'own', 's', 'same', 'she', 
    'should', 'so', 'some', 'such', 't', 'than', 'that', 'the', 'their', 'theirs', 'them', 
    'themselves', 'then', 'there', 'these', 'they', 'this', 'those', 'through', 'to', 'too',
     'under', 'until', 'up', 'very', 'was', 'we', 'were', 'what', 'when', 'where', 'which',
      'while', 'who', 'whom', 'why', 'will', 'with', 'you', 'your', 'yours', 'yourself', 
      'yourselves']

def preProcess(tweet):
    tweet = tweet.lower()
    clean_tweet= []
    final_tweet = []
    clean_tweet = re.sub(r'\^(RT)|(rt)','',tweet)      
    clean_tweet = re.sub(r'(\S)*www\.(\S)+','',clean_tweet)
    clean_tweet = re.sub(r'#', '', clean_tweet)
    clean_tweet = re.sub(r'@\w+','',clean_tweet)
    clean_tweet = clean_tweet.split()

    
    for word in clean_tweet:
        if word not in stopwords and word not in string.punctuation:
            word = stem(word)
            final_tweet.append(word)             
    
    return final_tweet   

#print(preProcess('asd ^RT ^rt asd www.asd.com asd https://www.asd.com asd http://www.asd.com asd #mondaymotivation asd @ashu I am a robot.'))

