import numpy as np
from preprocessor import preProcess

def buildFrequency(tweets,ys):
    # Convert np array to list since zip needs an iterable.
    # The squeeze is necessary or the list ends up with one element.
    
    ylist = np.squeeze(ys).tolist()
    
    freq = {}

    for tweet,y in zip(tweets,ylist):
        for word in preProcess(tweet):
            pair = (word,y)
            freq[pair] = freq.get(pair,0) + 1
            
    return freq

