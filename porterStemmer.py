# all letters except  a,e,i,o,u and ($y) where $ is a consonant.
def isConsonant(word,i):
    if word[i] not in'aeiou':
        if word[i] == 'y' and i == 0: 
            return 1
        elif word[i] == 'y' and word[i-1] in 'aeiou':
            return 1
        else:
            return 0
    else:
        return 0
    

#(C)(VC)^m(V)
def m(word):
    i = v = c = 0 
    #find number of time (c) came after (v).
    while(i<len(word)):
        if not isConsonant(word,i): 
            v = v*c + 1
        if v>0 and isConsonant(word,i):
            c = (v-c)*c + 1
        if c == 2:
            break
        i = i + 1
    return c

def v(word):
    for i in range(len(word)):
        if not isConsonant(word,i):
            return 1
    return 0

def stard(word):
    if len(word) < 2:
        return 0
    n = len(word)
    if isConsonant(word,n-1) and word[n-1] == word[n-2]:
        return 1 
    return 0

def staro(word):
    if len(word) < 3:
        return 0
    n = len(word)
    if isConsonant(word,n-1) and not isConsonant(word,n-2) and isConsonant(word,n-3):
        if word[n-1] != 'w' and word[n-1] != 'x' and word[n-1] != 'y':
            return 1
    return 0
    
def step1a(word):
    if word[-4:] == 'sses':
        word = word[:-4] + 'ss'
    elif word[-3:] == 'ies':
        word = word[:-3] + 'i'
    elif word[-1] == 's' and word[-2]!='s':
        word = word[:-1]
    return word

def step1b(word):
    flag = 0
    if m(word[:-3]) and word[-3:] == 'eed':
        word = word[:-1]
    elif v(word[:-2]) and word[-2:] == 'ed':
        word = word[:-2]        
        flag = 1
    elif v(word[:-3]) and word[-3:] == 'ing':
        word = word[:-3]
        flag = 1
   
    if (flag == 1):
        if word[-2:] == 'at' or word[-2:] == 'bl' or word[-2:] == 'iz':
            word = word + 'e'
        elif stard(word) and word[-1] not in 'lsz':
            word = word[:-1]
        elif m(word)==1 and staro(word):
            word = word + 'e'
    return word        

def step1c(word):
    if word[-1] == 'y' and v(word[:-1]):
        word = word[:-1] + 'i'
    return word

def step2(word):
    if m(word[:-7]) >0 and word[-7:] == 'ational':
        word = word[:-7] + 'ate'		    						
    elif m(word[:-6]) >0 and word[-6:] == 'tional':
        word = word[:-6] + 'tion'	    												    						
    elif m(word[:-4]) >0 and word[-4:] == 'enci':
        word = word[:-4] + 'ence'		    						
    elif m(word[:-4]) >0 and word[-4:] == 'anci':
        word = word[:-4] + 'ance'		    						
    elif m(word[:-4]) >0 and word[-4:] == 'izer':
        word = word[:-4] + 'ize'		    		    				
    elif m(word[:-4]) >0 and word[-4:] == 'abli':
        word = word[:-4] + 'able'		    						
    elif m(word[:-4]) >0 and word[-4:] == 'alli':
        word = word[:-4] + 'al'		    						
    elif m(word[:-5]) >0 and word[-5:] == 'entli':
        word = word[:-5] + 'ent'	    						
    elif m(word[:-3]) >0 and word[-3:] == 'eli':
        word = word[:-3] + 'e'		    						
    elif m(word[:-5]) >0 and word[-5:] == 'ousli':
        word = word[:-5] + 'ous'		    						
    elif m(word[:-7]) >0 and word[-7:] == 'ization':
        word = word[:-7] + 'ize'		    						
    elif m(word[:-5]) >0 and word[-5:] == 'ation':
        word = word[:-5] + 'ate'		    						
    elif m(word[:-4]) >0 and word[-4:] == 'ator':
        word = word[:-4] + 'ate'		    						
    elif m(word[:-5]) >0 and word[-5:] == 'alism':
        word = word[:-5] + 'al'		    						
    elif m(word[:-7]) >0 and word[-7:] == 'iveness':
        word = word[:-7] + 'ive'		    						
    elif m(word[:-7]) >0 and word[-7:] == 'fulness':
        word = word[:-7] + 'ful'		    						
    elif m(word[:-7]) >0 and word[-7:] == 'ousness':
        word = word[:-7] + 'ous'		    						
    elif m(word[:-5]) >0 and word[-5:] == 'aliti':
        word = word[:-5] + 'al'		    						
    elif m(word[:-5]) >0 and word[-5:] == 'iviti':
        word = word[:-5] + 'ive'		    						
    elif m(word[:-6]) >0 and word[-6:] == 'biliti':
        word = word[:-6] + 'ble'
    return word

def step3(word):
    if m(word[:-5]) >0 and word[-5:] == 'icate':
        word = word[:-5] + 'ic'		    						
    elif m(word[:-5]) >0 and word[-5:] == 'ative':
        word = word[:-5] + ''	    												    						
    elif m(word[:-5]) >0 and word[-5:] == 'alize':
        word = word[:-5] + 'al'		    						
    elif m(word[:-5]) >0 and word[-5:] == 'iciti':
        word = word[:-5] + 'ic'		    						
    elif m(word[:-4]) >0 and word[-4:] == 'ical':
        word = word[:-4] + 'ic'		    		    				
    elif m(word[:-3]) >0 and word[-3:] == 'ful':
        word = word[:-3] + ''		    						
    elif m(word[:-4]) >0 and word[-4:] == 'ness':
        word = word[:-4] + ''
    return word

def step4(word):
    if m(word[:-2]) >1 and word[-2:] == 'al':
        word = word[:-2] + ''		    						
    elif m(word[:-4]) >1 and word[-4:] == 'ance':
        word = word[:-4] + ''	    												    						
    elif m(word[:-4]) >1 and word[-4:] == 'ence':
        word = word[:-4] + ''		    						
    elif m(word[:-2]) >1 and word[-2:] == 'er':
        word = word[:-2] + ''		    						
    elif m(word[:-2]) >1 and word[-2:] == 'ic':
        word = word[:-2] + ''		    		    				
    elif m(word[:-4]) >1 and word[-4:] == 'able':
        word = word[:-4] + ''		    						
    elif m(word[:-4]) >1 and word[-4:] == 'ible':
        word = word[:-4] + ''		    						
    elif m(word[:-3]) >1 and word[-3:] == 'ant':
        word = word[:-3] + ''	    						
    elif m(word[:-5]) >1 and word[-5:] == 'ement':
        word = word[:-5] + ''		    						
    elif m(word[:-4]) >1 and word[-4:] == 'ment':
        word = word[:-4] + ''		    						
    elif m(word[:-3]) >1 and word[-3:] == 'ent':
        word = word[:-3] + ''		    						
    elif m(word[:-3]) >1 and word[-3:] == 'ion':
        word = word[:-3] + ''		    						
    elif m(word[:-2]) >1 and word[-2:] == 'ou':
        word = word[:-2] + ''		    						
    elif m(word[:-3]) >1 and word[-3:] == 'ism':
        word = word[:-3] + ''		    						
    elif m(word[:-3]) >1 and word[-3:] == 'ate':
        word = word[:-3] + ''		    						
    elif m(word[:-3]) >1 and word[-3:] == 'iti':
        word = word[:-3] + ''		    						
    elif m(word[:-3]) >1 and word[-3:] == 'ous':
        word = word[:-3] + ''		    						
    elif m(word[:-3]) >1 and word[-3:] == 'ive':
        word = word[:-3] + ''		    						
    elif m(word[:-3]) >1 and word[-3:] == 'ize':
        word = word[:-3] + ''		    						    
    return word

def step5a(word):
    if m(word[:-1]) > 1 and word[-1] == 'e':
        word = word[:-1] + ''
    if m(word[:-1]) == 1 and not staro(word[:-1]) and word[-1] == 'e':
        word = word[:-1] + ''
    return word

def step5b(word):
    if m(word) > 1 and stard(word) and word[-1] == 'l':
        word = word[:-1]
    return word

def stem(word):
    word = step1a(word)
    word = step1b(word)
    word = step1c(word)
    word = step2(word)
    word = step3(word)
    word = step4(word)
    word = step5a(word)
    word = step1b(word)
    return word
