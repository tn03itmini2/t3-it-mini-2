#!/usr/bin/env python
# coding: utf-8

# #  Logistic Regression 

# In[1]:


import numpy as np
import pandas as pd
import math
from nltk.corpus import twitter_samples 

from preprocessor import preProcess 
from buildFrequency import buildFrequency


# ## Prepare NLTK test dataset

# In[2]:


positive_tweets = twitter_samples.strings('positive_tweets.json')
negative_tweets = twitter_samples.strings('negative_tweets.json')


# In[3]:


train_x = positive_tweets[:3500] + negative_tweets[:3500]
valid_x = positive_tweets[3500:4250] + negative_tweets[3500:4250]
test_x = positive_tweets[4250:] + negative_tweets[4250:]


# In[4]:


train_y = np.append(np.ones((3500, 1)), np.zeros((3500, 1)), axis=0)
valid_y = np.append(np.ones((750,1)), np.zeros((750,1)), axis=0)
test_y = np.append(np.ones((750, 1)), np.zeros((750, 1)), axis=0)


# ## Prepare Kaggle Dataset

# In[5]:


col_list = ["Y", "X"]
kaggle_dataset  = pd.read_csv('kaggle_balanced_dataset.csv',encoding = "Latin-1",usecols=col_list)


# In[6]:


KAGGLE_X = kaggle_dataset["X"]
KAGGLE_Y = kaggle_dataset["Y"]


# ## Create Functions

# In[7]:


def sigmoid(z): 
    h = 1/(1+np.exp(-z))
    return h


# In[8]:


def gradientDescent(x, y, theta, alpha, iterations):
    m = len(x)
    
    for i in range(0, iterations):

        z = np.dot(x,theta)
        h = sigmoid(z)
        
        yt = y.transpose()
        y2 = 1-y
        y2t = y2.transpose()
        
        J = -1/m*(np.dot(yt,np.log(h))+ np.dot(y2t,np.log(1-h))) 

        xt = x.transpose()        
        gradient = 1/m*np.dot(xt,(h-y))
        theta = theta - alpha*gradient
        
    return J, theta


# In[9]:


def extract_features(tweet, frequency_dictionary):
    word_list = preProcess(tweet)
    
    # 3 elements in the form of a 1 x 3 vector
    x = np.zeros((1, 3)) 
    #bias term is set to 1
    x[0,0] = 1 
    flag = 0
    for word in word_list: 
        if word in ['not','no','nor','never','neither']:
            flag = 1
            continue
        if flag == 0:
            x[0,1] += frequency_dictionary.get((word,1),0) 
            x[0,2] += frequency_dictionary.get((word,0),0) 
        elif flag == 1:
            x[0,1] += frequency_dictionary.get((word,0),0)      
            x[0,2] += frequency_dictionary.get((word,1),0)
        flag = 0         
    return x


# In[10]:


def predict_tweet(tweet, frequency_dictionary, theta):
    x = extract_features(tweet,frequency_dictionary)

    y_pred = sigmoid(np.dot(x,theta))
    
    return y_pred


# In[11]:


def test_logistic_regression(test_x, test_y, frequency_dictionary, theta, confusion = 0):
    y_hat = []
    m = len(test_y)
    accuracy = 0
    confusion_matrix = []
    tp = tn = fp = fn = 0
    for tweet,y_actual in zip(test_x,test_y):
        y_pred = 1 if predict_tweet(tweet, frequency_dictionary, theta)>0.5 else 0 
        
        if y_pred == y_actual:
            accuracy +=1
        if confusion == 1:
            if y_pred == 1 and y_pred == y_actual:
                tp += 1
            elif y_pred == 0 and y_pred == y_actual:
                tn += 1
            elif y_pred == 1 and y_pred != y_actual:
                fp += 1
            elif y_pred == 0 and y_pred != y_actual:
                fn += 1
                
    accuracy = accuracy/m
    confusion_matrix.extend([tn, fp, fn, tp])
    if confusion == 1:
        return accuracy, confusion_matrix
    return accuracy


# ## Build Frequency

# In[12]:


frequency_dictionary = buildFrequency(train_x, train_y)


# In[13]:


sorted_freq_dict = sorted(frequency_dictionary.items(), key=lambda x: x[1], reverse=True)


# # Validation Testing

# In[14]:


def validation_testing(hyperparameter):
    hyper_freq_dict = {}
    for i in range(int(hyperparameter/100*len(sorted_freq_dict))):
        hyper_freq_dict[sorted_freq_dict[i][0]] = sorted_freq_dict[i][1]
        
    X = np.zeros((len(train_x), 3))
    for i in range(len(train_x)):
        X[i, :]= extract_features(train_x[i], hyper_freq_dict)
    Y = train_y
    
    # Apply gradient descent
    J, theta = gradientDescent(X, Y, np.zeros((3, 1)), 1e-9, 1500)
    
    accuracy = test_logistic_regression(valid_x, valid_y, hyper_freq_dict, theta)
    print(f"Logistic regression model's accuracy on Hyperparameter = {hyperparameter} is {accuracy:.4f} and cost = {J}.")


# In[15]:


for i in [10,20,30,40,50,60,70,80,90,100]:
    validation_testing(i)


# In[16]:


for i in range(1,11):
    validation_testing(i)


# ## Final Parameters

# In[17]:


final_hyperparameter = 15
final_freq_dict = {}
for i in range(int(final_hyperparameter/100*len(sorted_freq_dict))):
    final_freq_dict[sorted_freq_dict[i][0]] = sorted_freq_dict[i][1]
X = np.zeros((len(train_x), 3)) 
for i in range(len(train_x)):
    X[i, :]= extract_features(train_x[i], final_freq_dict )
Y = train_y

# Apply gradient descent
J, theta = gradientDescent(X, Y, np.zeros((3, 1)), 1e-9, 1000)
print(f"The cost after training is {J}.")


# ## Testing on NLTK

# In[20]:


accuracy, confusion_matrix = test_logistic_regression(test_x, test_y, final_freq_dict, theta, 1)
print(f"Logistic regression model's accuracy = {accuracy:.4f}")
print("Confusion Matrix")
print(f"NA \t Predicted_0 \t Predicted_1")
print(f"Actual_0 \t {confusion_matrix[0]} \t {confusion_matrix[1]} ")
print(f"Actual_1 \t {confusion_matrix[2]} \t {confusion_matrix[3]} ")


# # Testing on kaggle

# In[23]:


accuracy, confusion_matrix = test_logistic_regression(KAGGLE_X , KAGGLE_Y, final_freq_dict, theta, 1)
print(f"Logistic regression model's accuracy = {accuracy:.4f}")
print("Confusion Matrix")
print(f"NA \t Predicted_0 \t Predicted_1")
print(f"Actual_0 \t {confusion_matrix[0]} \t {confusion_matrix[1]} ")
print(f"Actual_1 \t {confusion_matrix[2]} \t {confusion_matrix[3]} ")

