#!/usr/bin/env python
# coding: utf-8

# #  Naive Bayes 

# In[24]:


import numpy as np
import pandas as pd
import string 
from nltk.corpus import twitter_samples

from preprocessor import preProcess 
from buildFrequency import buildFrequency


# In[2]:


positive_tweets = twitter_samples.strings('positive_tweets.json')
negative_tweets = twitter_samples.strings('negative_tweets.json')


# In[3]:


positive_tweets[:5]


# In[4]:


train_x = positive_tweets[:3500] + negative_tweets[:3500]
valid_x = positive_tweets[3500:4250] + negative_tweets[3500:4250]
test_x = positive_tweets[4250:] + negative_tweets[4250:]


# In[5]:


train_y = np.append(np.ones((3500, 1)), np.zeros((3500, 1)), axis=0)
valid_y = np.append(np.ones((750,1)), np.zeros((750,1)), axis=0)
test_y = np.append(np.ones((750, 1)), np.zeros((750, 1)), axis=0)


# ## Kaggle Dataset

# In[6]:


col_list = ["Y", "X"]
kaggle_dataset  = pd.read_csv('kaggle_balanced_dataset.csv',encoding = "Latin-1",usecols=col_list)


# In[7]:


KAGGLE_X = kaggle_dataset["X"]
KAGGLE_Y = kaggle_dataset["Y"]


# In[8]:


len(KAGGLE_X)


# ## Create Functions

# In[9]:


# find logLikelihood of each word in vocabulary and find logprior. 
def naive_bayes(frequency_dictionary,train_x,train_y):
    # return values
    logLikelihood = {}
    logPrior = 0
    # vocabulary is list of unique words in dictionary.
    vocabulary = set()
    # number of words in positive tweet and negative tweets.
    N_positive = N_negative = 0
    
    for key_tuple in frequency_dictionary.keys():
        vocabulary.add(key_tuple[0])
        
        if key_tuple[1] == 1:
            N_positive += 1
        else:
            N_negative += 1
    V = len(vocabulary)
    
    # D means Documents
    D = len(train_x)
    D_positive = np.sum(train_y)
    D_negative = D - D_positive
    
    logPrior = np.log(D_positive) - np.log(D_negative)
    
    # find logLikelihood of each word 
    for word in vocabulary:
        positive_frequency = frequency_dictionary.get((word,1),0)
        negative_frequency = frequency_dictionary.get((word,0),0)
        
        positive_probability = (positive_frequency+1)/(N_positive + V)
        negative_probability = (negative_frequency+1)/(N_negative + V)
        
        logLikelihood[word] = np.log(positive_probability/negative_probability)
    
    return logPrior,logLikelihood


# In[10]:


def predict_tweet(tweet,logPrior,logLikelihood):
    tweet_vocabulary = preProcess(tweet)
    probability = logPrior
    flag = 0
    # add logLikelihood of each word to probability.
    for word in tweet_vocabulary:
        if word in ['not','never','neither','nor']:
            flag = 1
            continue
        if flag == 0:
            #print(flag)
            probability += logLikelihood.get(word,0) 
        elif flag == 1:
            #print(flag)
            e_log = np.exp(logLikelihood.get(word,0) )
            probability += 0 if e_log == 1.0 else 1/e_log 
        flag = 0 

    return probability


# In[21]:


def test(test_x,test_y,logPrior,logLikelihood, confusion = 0):
    accuracy = 0
    m = len(test_x)
    tp = tn = fp = fn = 0
    confusion_matrix = []
    for i in range(m):
        y_hat = 1 if predict_tweet(test_x[i],logPrior,logLikelihood) > 0 else 0
        if y_hat == test_y[i]:
            accuracy +=1
        if confusion == 1:
            if y_hat == 1 and y_hat == test_y[i]:
                tp += 1
            elif y_hat == 1 and y_hat != test_y[i]:
                fp += 1
            elif y_hat == 0 and y_hat == test_y[i]:
                tn += 1
            elif y_hat == 0 and y_hat != test_y[i]:
                fn += 1
    accuracy /=m
    confusion_matrix.extend([tn, fp, fn, tp])
    if confusion == 1:
        return accuracy, confusion_matrix
    return accuracy 
    


# ## Build Frequency

# In[12]:


frequency_dictionary = buildFrequency(train_x, train_y)


# In[13]:


sorted_freq_dict = sorted(frequency_dictionary.items(), key=lambda x: x[1], reverse=True)


# In[14]:


print(sorted_freq_dict[0:5])
print(sorted_freq_dict[-5:])


# ## Validation Testing

# In[15]:


def validation_testing(hyperparameter):
    hyper_freq_dict = {}
    for i in range(int(hyperparameter/100*len(sorted_freq_dict))):
        hyper_freq_dict[sorted_freq_dict[i][0]] = sorted_freq_dict[i][1]
    logPrior,logLikelihood = naive_bayes(hyper_freq_dict,valid_x,valid_y)
    accuracy = test(valid_x,valid_y,logPrior,logLikelihood)
    print(f"Naive Bayes model's accuracy for hyperparameter = {hyperparameter} is {accuracy:.4f}")


# In[16]:


for i in [10,20,30,40,50,60,70,80,90,100]:
    validation_testing(i)


# In[17]:


for i in range(20,35):
    validation_testing(i)


# ## Final Parameters

# In[18]:


final_hyperparameter = 26
final_freq_dict = {}
for i in range(int(final_hyperparameter/100*len(sorted_freq_dict))):
    final_freq_dict[sorted_freq_dict[i][0]] = sorted_freq_dict[i][1]


# In[19]:


logPrior,logLikelihood = naive_bayes(final_freq_dict,train_x,train_y)


# ## Testing on NLTK

# In[22]:


accuracy, confusion_matrix = test(train_x,train_y,logPrior,logLikelihood, 1)
print(f"Naive Bayes model's accuracy is {accuracy:.4f}")
print(f"NA \t Predicted_0 \t Predicted_1")
print(f"Actual_0 \t {confusion_matrix[0]} \t {confusion_matrix[1]} ")
print(f"Actual_1 \t {confusion_matrix[2]} \t {confusion_matrix[3]} ")


# ## Testing on Kaggle

# In[23]:


accuracy, confusion_matrix = test(KAGGLE_X,KAGGLE_Y,logPrior,logLikelihood, 1)
print(f"Naive Bayes model's accuracy is {accuracy:.4f}")
print(f"NA \t Predicted_0 \t Predicted_1")
print(f"Actual_0 \t {confusion_matrix[0]} \t {confusion_matrix[1]} ")
print(f"Actual_1 \t {confusion_matrix[2]} \t {confusion_matrix[3]} ")

