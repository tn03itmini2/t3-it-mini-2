#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
import math
from nltk.corpus import twitter_samples 

# Our own build files
from preprocessor import preProcess 
from buildFrequency import buildFrequency


# In[2]:


# select the set of positive and negative tweets
all_positive_tweets = twitter_samples.strings('positive_tweets.json')
all_negative_tweets = twitter_samples.strings('negative_tweets.json')


# In[3]:


test_pos = all_positive_tweets[4000:]
train_pos = all_positive_tweets[:4000]
test_neg = all_negative_tweets[4000:]
train_neg = all_negative_tweets[:4000]

train_x = train_pos + train_neg 
test_x = test_pos + test_neg


# In[4]:


train_y = np.append(np.ones((len(train_pos), 1)), np.zeros((len(train_neg), 1)), axis=0)
test_y = np.append(np.ones((len(test_pos), 1)), np.zeros((len(test_neg), 1)), axis=0)


# In[5]:


frequency_dictionary = buildFrequency(train_x, train_y)
        


# In[6]:


frequency_dictionary


# In[7]:


def extract_features(tweet, frequency_dictionary):
    word_list = preProcess(tweet)
    
    # 3 elements in the form of a 1 x 3 vector
    x = np.zeros((1, 3)) 
    #bias term is set to 1
    x[0,0] = 1
    flag = 0
    for word in word_list: 
        if word in ['not','never','neither','nor']:
            flag = 1
            continue
        if flag == 0:
            x[0,1] += frequency_dictionary.get((word,1),0) 
            x[0,2] += frequency_dictionary.get((word,0),0) 
        elif flag == 1:
            x[0,1] += frequency_dictionary.get((word,0),0)      
            x[0,2] += frequency_dictionary.get((word,1),0)
        flag = 0
    return x


# In[9]:


X = np.zeros((len(train_x), 3))
for i in range(len(train_x)):
    X[i, :]= extract_features(train_x[i], frequency_dictionary)


# In[10]:


# Vectorized matrix.
X

